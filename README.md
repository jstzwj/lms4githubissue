# LMs4GitHubIssue

This is the source code for the paper "How Well Do Pre-trained Contextual Language Representations Recommend Labels for GitHub Issues?".

## Package Requirement

To run this code, some packages are needed as following:

```
transformers==4.5.1
allennlp==2.4.0
tqdm==4.60.0
pytorch_lightning==1.3.2
spacy==3.0.6
overrides==3.1.0
pandas==1.2.4
torchmetrics==0.3.2
numpy==1.19.4
torch==1.8.0
gensim==4.0.1
datasets==1.7.0
scikit-posthocs==0.6.7
```

## Prepare dataset

**data_view.7z**

Under the data root is the folder for different projects, the names of projects follow "{owner}_{name}".

Under the folder of each project, the dataset follows the name "{owner}_{name}.txt". The format of the dataset is JSON LInes.

## RQ1 How well can deep learning pre-trained classifiers recommend labels for GitHub issues?

This section introduces how to train and test different deep learning classifiers to recommend labels for GitHub issues.

### Train and test

```
sh run_rq1.sh
```

The run_rq1.sh contains deep learning models with four pretrained models.

The result will be saved under output/rq1/



## RQ2 Does software development specific data help pre-trained models improve recommendation compared to other pre-trained data set?

Pretrained BERT model:

```
sh further_pretrain.sh
```

Then the checkpoints of further pre-trained models are saved under `output/github-mlm ` and `output/github-mlm`.

Test checkpoints:

```
python further_step.py
```

## References

For more details about data processing, please refer to the `code comments` and our paper.

For more flexible and specific parameter settings during training, please refer to the tutorial of *pytorch* and *pytorch_lightning*.



