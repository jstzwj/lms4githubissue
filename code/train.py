
import argparse
import json
import random
import glob
import tqdm
import os

import pandas as pd

from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
# from sklearn.model_selection import KFold

from LMs4GitHubIssue.dataset.issue_dataset import IssueDataset
from LMs4GitHubIssue.dataset.allennlp_issue_dataset import AllennlpIssueDatasetReader
from LMs4GitHubIssue.models.textcnn import TextCNN
from LMs4GitHubIssue.models.bilstm import BiLSTM
from LMs4GitHubIssue.models.rcnn import RCNN
from LMs4GitHubIssue.models.bert import Bert
from LMs4GitHubIssue.models.model import TextLabelRecModel
from LMs4GitHubIssue.util.mem import occumpy_mem

from LMs4GitHubIssue.tokenizer.allennlp_tokenizer import AllennlpTokenizer
# from allennlp.data.instance import Instance
# from allennlp.data.fields import Field, TextField, LabelField
# from allennlp.data.tokenizers import Token
from allennlp.data.vocabulary import Vocabulary
from allennlp.data.tokenizers.spacy_tokenizer import SpacyTokenizer
from allennlp.data.token_indexers.single_id_token_indexer import SingleIdTokenIndexer
from allennlp.modules.token_embedders.embedding import Embedding

def build_vocab(data, tokenizer):
    words = set()
    for d in data:
        tokens = tokenizer.tokenize(d['title'] + ' ' + d['description'])
        for t in tokens:
            words.add(t)
    return list(words)


def train_single(data_path: str, model_name: str, embedding_type=None, device=0):
    data = []
    with open(data_path, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(json.loads(line))

    random.shuffle(data)

    split_1 = int(0.8 * len(data))
    split_2 = int(0.9 * len(data))

    train_data = data[:split_1]
    valid_data = data[:split_2]
    test_data = data[split_2:]

    # init tokenizer
    if model_name in ["textcnn", "bilstm", "rcnn"]:
        # build vocab
        allennlp_tokenizer = SpacyTokenizer()
        allennlp_token_indexer = SingleIdTokenIndexer(token_min_padding_length=8, lowercase_tokens=True)
        allennlp_datareader = AllennlpIssueDatasetReader(allennlp_tokenizer, {'tokens': allennlp_token_indexer})
        vocab = Vocabulary.from_instances(allennlp_datareader.read(data_path))
        
        tokenizer = AllennlpTokenizer(vocab, allennlp_tokenizer, allennlp_token_indexer)
    elif model_name == "bert":
        from transformers import BertTokenizer
        tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    else:
        raise Exception("unknown model")
    
    # batch size
    if model_name == "textcnn":
        batch_size = 256
    elif model_name == "bilstm":
        batch_size = 256
    elif model_name == "rcnn":
        batch_size = 256
    elif model_name == "bert":
        batch_size = 32

    # init embedding
    token_embedding = None
    if embedding_type is not None:
        if embedding_type == 'glove':
            token_embedding = Embedding(num_embeddings=vocab.get_vocab_size('tokens'),
                                embedding_dim=300,
                                pretrained_file='embed/glove.6B/glove.6B.300d.txt',
                                vocab=vocab).weight.data
        elif embedding_type == 'word2vec':
            token_embedding = Embedding(num_embeddings=vocab.get_vocab_size('tokens'),
                                embedding_dim=300,
                                pretrained_file='embed/word2vec/word2vec-google-news-300.txt',
                                vocab=vocab).weight.data
        elif embedding_type == 'fasttext':
            token_embedding = Embedding(num_embeddings=vocab.get_vocab_size('tokens'),
                                embedding_dim=300,
                                pretrained_file='embed/fasttext/wiki.en.vec',
                                vocab=vocab).weight.data
        elif embedding_type.lower() == 'none':
            print('no pretrained embeddings')
        else:
            print('unknown embeddings')
    
    # label num
    all_labels = set()
    for obj in data:
        for c in obj['labels']:
            all_labels.add(c)

    all_labels = sorted(list(all_labels))

    # init dataset
    train_dataset = IssueDataset(train_data, all_labels, tokenizer)
    valid_dataset = IssueDataset(valid_data, all_labels, tokenizer)
    test_dataset = IssueDataset(test_data, all_labels, tokenizer)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, num_workers=8, shuffle=True)
    valid_loader = DataLoader(valid_dataset, batch_size=batch_size, num_workers=8)
    test_loader = DataLoader(test_dataset, batch_size=4, num_workers=8)

    # init model
    class_num=len(all_labels)
    if model_name == "textcnn":
        model = TextCNN(num_classes=class_num, vocab_size=vocab.get_vocab_size(), embedding_size=300, word_embeddings=token_embedding)
    elif model_name == "bilstm":
        model = BiLSTM(num_classes=class_num, vocab_size=vocab.get_vocab_size(), embedding_size=300, word_embeddings=token_embedding)
    elif model_name == "rcnn":
        model = RCNN(num_classes=class_num, vocab_size=vocab.get_vocab_size(), embedding_size=300, word_embeddings=token_embedding)
    elif model_name == "bert":
        model = Bert(num_classes=class_num)
    else:
        raise Exception("unknown model")

    # train
    trainer = pl.Trainer(
        # accelerator='ddp',
        amp_backend='native',
        amp_level='O2',
        gpus=[device],
        callbacks=[EarlyStopping(monitor='val_loss')],
        checkpoint_callback=False
    )
    trainer.fit(model,
        train_dataloader=train_loader,
        val_dataloaders=[valid_loader],
        )

    ret = trainer.test(model, test_dataloaders=test_loader)
    return ret[0]

def main():
    parser = argparse.ArgumentParser(description='Training parameters.')
    parser.add_argument('--device', default=0, type=int, required=False, help='使用的实验设备, -1:CPU, >=0:GPU')
    parser.add_argument('--model', default='textcnn', type=str, required=False, help='模型名称')
    parser.add_argument('--embed', default='glove', type=str, required=False, help='词嵌入')
    args = parser.parse_args()
    print('args:\n' + args.__repr__())

    # 占用全部显存
    occumpy_mem(str(args.device))

    out_name = f"output/rq1/{args.model}_{args.embed}_out.csv"
    files = glob.glob("./data/data_view100/*/filter_dataset.txt")
    files = sorted(files)

    if os.path.exists(out_name):
        metric_dict_df = pd.read_csv(out_name)
        metric_dict = metric_dict_df.to_dict(orient="list")
    else:
        metric_dict = {
            'test_acc_1_epoch': [],
            'test_precision_1_epoch': [],
            'test_recall_1_epoch': [],

            'test_acc_2_epoch': [],
            'test_precision_2_epoch': [],
            'test_recall_2_epoch': [],

            'test_acc_3_epoch': [],
            'test_precision_3_epoch': [],
            'test_recall_3_epoch': [],

            'test_acc_4_epoch': [],
            'test_precision_4_epoch': [],
            'test_recall_4_epoch': [],

            'test_acc_5_epoch': [],
            'test_precision_5_epoch': [],
            'test_recall_5_epoch': [],
        }
        metric_dict_df = pd.DataFrame(metric_dict)
    for i, file in enumerate(tqdm.tqdm(files)):
        random.seed(hash(file))
        print(file)
        if i < len(metric_dict_df):
            continue
        each_metrics = train_single(file, args.model, args.embed, args.device)
        for k, v in each_metrics.items():
            if k in metric_dict:
                metric_dict[k].append(v)

        df = pd.DataFrame(metric_dict)
        df.to_csv(out_name, index=False)

if __name__ == "__main__":
    main()