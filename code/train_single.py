
import argparse
import json
import random
from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

from LMs4GitHubIssue.dataset.issue_dataset import IssueDataset
from LMs4GitHubIssue.models.textcnn import TextCNN
from LMs4GitHubIssue.models.bert import Bert
from LMs4GitHubIssue.models.model import TextLabelRecModel

def train_single(data_path: str, model_name: str):
    data = []
    with open(data_path, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(json.loads(line))

    random.shuffle(data)

    split_1 = int(0.8 * len(data))
    split_2 = int(0.9 * len(data))

    train_data = data[:split_1]
    valid_data = data[:split_2]
    test_data = data[split_2:]

    # init tokenizer
    if model_name == "textcnn":
        tokenizer = None
    elif model_name == "bert":
        from transformers import BertTokenizer
        tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    else:
        raise Exception("unknown model")
    
    # label num
    all_labels = set()
    for obj in data:
        for c in obj['labels']:
            all_labels.add(c)

    all_labels = sorted(list(all_labels))

    # init dataset
    train_dataset = IssueDataset(train_data, all_labels, tokenizer)
    valid_dataset = IssueDataset(valid_data, all_labels, tokenizer)
    test_dataset = IssueDataset(test_data, all_labels, tokenizer)

    train_loader = DataLoader(train_dataset, batch_size=2, num_workers=8, shuffle=True)
    valid_loader = DataLoader(valid_dataset, batch_size=2, num_workers=8)
    test_loader = DataLoader(test_dataset, batch_size=1, num_workers=8)

    # init model
    class_num=len(all_labels)
    if model_name == "textcnn":
        torch_model = TextCNN()
        model = TextLabelRecModel(torch_model)
    elif model_name == "bert":
        model = Bert(num_classes=class_num)
    else:
        raise Exception("unknown model")

    # train
    trainer = pl.Trainer(
        accelerator='ddp',
        amp_backend='native',
        amp_level='O2',
        gpus=[0],
        callbacks=[EarlyStopping(monitor='val_loss')]
    )
    trainer.fit(model,
        train_dataloader=train_loader,
        val_dataloaders=[valid_loader],
        )

    ret = trainer.test(model, test_dataloaders=test_loader)
    print(ret)

def main():
    train_single("data/data_view100/adam-p_markdown-here/filter_dataset.txt", "bert")

if __name__ == "__main__":
    main()