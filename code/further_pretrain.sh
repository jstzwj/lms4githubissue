python run_mlm.py \
    --model_name_or_path bert-base-uncased \
    --train_file data/pretrained_so/train.txt \
    --validation_file data/pretrained_so/valid.txt \
    --do_train \
    --do_eval \
    --line_by_line \
    --save_steps 2000 \
    --output_dir ./output/so-mlm

python run_mlm.py \
    --model_name_or_path bert-base-uncased \
    --train_file data/pretrained_github/train.txt \
    --validation_file data/pretrained_github/valid.txt \
    --do_train \
    --do_eval \
    --line_by_line \
    --save_steps 2000 \
    --output_dir ./output/github-mlm