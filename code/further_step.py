'''
该脚本用来研究bert不同pre-train步数后在某数据集上的效果提升
'''

import argparse
import json
import random
import glob
import tqdm
import os

import pandas as pd

from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
# from sklearn.model_selection import KFold

from LMs4GitHubIssue.dataset.issue_dataset import IssueDataset
from LMs4GitHubIssue.dataset.allennlp_issue_dataset import AllennlpIssueDatasetReader
from LMs4GitHubIssue.models.textcnn import TextCNN
from LMs4GitHubIssue.models.bilstm import BiLSTM
from LMs4GitHubIssue.models.rcnn import RCNN
from LMs4GitHubIssue.models.bert import Bert
from LMs4GitHubIssue.models.model import TextLabelRecModel

from LMs4GitHubIssue.tokenizer.allennlp_tokenizer import AllennlpTokenizer
# from allennlp.data.instance import Instance
# from allennlp.data.fields import Field, TextField, LabelField
# from allennlp.data.tokenizers import Token
from allennlp.data.vocabulary import Vocabulary
from allennlp.data.tokenizers.spacy_tokenizer import SpacyTokenizer
from allennlp.data.token_indexers.single_id_token_indexer import SingleIdTokenIndexer
from allennlp.modules.token_embedders.embedding import Embedding

def build_vocab(data, tokenizer):
    words = set()
    for d in data:
        tokens = tokenizer.tokenize(d['title'] + ' ' + d['description'])
        for t in tokens:
            words.add(t)
    return list(words)


def train_single(data_path: str, model_name: str, device=0):
    data = []
    with open(data_path, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(json.loads(line))

    random.shuffle(data)

    split_1 = int(0.8 * len(data))
    split_2 = int(0.9 * len(data))

    train_data = data[:split_1]
    valid_data = data[:split_2]
    test_data = data[split_2:]

    # init tokenizer
    from transformers import BertTokenizerFast
    # export TOKENIZERS_PARALLELISM=false
    tokenizer = BertTokenizerFast.from_pretrained("bert-base-uncased")
    
    # batch size
    batch_size = 32
    
    # label num
    all_labels = set()
    for obj in data:
        for c in obj['labels']:
            all_labels.add(c)

    all_labels = sorted(list(all_labels))

    # init dataset
    train_dataset = IssueDataset(train_data, all_labels, tokenizer)
    valid_dataset = IssueDataset(valid_data, all_labels, tokenizer)
    test_dataset = IssueDataset(test_data, all_labels, tokenizer)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, num_workers=8, shuffle=True)
    valid_loader = DataLoader(valid_dataset, batch_size=batch_size, num_workers=8)
    test_loader = DataLoader(test_dataset, batch_size=4, num_workers=8)

    # init model
    class_num=len(all_labels)
    model = Bert(num_classes=class_num, model_name=model_name)

    # train
    trainer = pl.Trainer(
        # accelerator='ddp',
        amp_backend='native',
        amp_level='O2',
        gpus=[device],
        callbacks=[EarlyStopping(monitor='val_loss')],
        checkpoint_callback=False,
        check_val_every_n_epoch=3,
        limit_val_batches=200
    )
    trainer.fit(model,
        train_dataloader=train_loader,
        val_dataloaders=[valid_loader],
        )

    ret = trainer.test(model, test_dataloaders=test_loader)
    return ret[0]

def main():
    parser = argparse.ArgumentParser(description='Training parameters.')
    parser.add_argument('--device', default=0, type=int, required=False, help='使用的实验设备, -1:CPU, >=0:GPU')
    parser.add_argument('--save_id', default=0, type=int, required=False, help='实验id')
    parser.add_argument('--data_path', default='data/data_view100/tensorflow_tensorflow/filter_dataset.txt', type=str, required=False, help='数据集目录')
    parser.add_argument('--model_path', default='output/so-mlm', type=str, required=False, help='模型目录')
    args = parser.parse_args()
    print('args:\n' + args.__repr__())

    out_name = f"{args.model_path.split('/')[-1]}_{args.save_id}.csv"

    if os.path.exists(out_name):
        metric_dict_df = pd.read_csv(out_name)
        metric_dict = metric_dict_df.to_dict(orient="list")
    else:
        metric_dict = {
            'step': [],

            'test_acc_1_epoch': [],
            'test_precision_1_epoch': [],
            'test_recall_1_epoch': [],

            'test_acc_2_epoch': [],
            'test_precision_2_epoch': [],
            'test_recall_2_epoch': [],

            'test_acc_3_epoch': [],
            'test_precision_3_epoch': [],
            'test_recall_3_epoch': [],

            'test_acc_4_epoch': [],
            'test_precision_4_epoch': [],
            'test_recall_4_epoch': [],

            'test_acc_5_epoch': [],
            'test_precision_5_epoch': [],
            'test_recall_5_epoch': [],
        }
        metric_dict_df = pd.DataFrame(metric_dict)
    
    count = 0
    for step in tqdm.tqdm(range(2000, 100000, 2000)):
        for i in tqdm.tqdm(range(1), leave=False):
            if i < len(metric_dict_df):
                continue
            model_path = os.path.join(args.model_path, f'checkpoint-{step}')
            each_metrics = train_single(args.data_path, model_path, args.device)

            metric_dict['step'].append(step)
            for k, v in each_metrics.items():
                if k in metric_dict:
                    metric_dict[k].append(v)

            df = pd.DataFrame(metric_dict)
            df.to_csv(out_name, index=False)

            count += 1
        

if __name__ == "__main__":
    main()