DEVICE=0

python train.py --model textcnn --embed none --device $DEVICE
python train.py --model textcnn --embed glove --device $DEVICE
python train.py --model textcnn --embed word2vec --device $DEVICE
python train.py --model textcnn --embed fasttext --device $DEVICE

python train.py --model bilstm --embed none --device $DEVICE
python train.py --model bilstm --embed glove --device $DEVICE
python train.py --model bilstm --embed word2vec --device $DEVICE
python train.py --model bilstm --embed fasttext --device $DEVICE

python train.py --model rcnn --embed none --device $DEVICE
python train.py --model rcnn --embed glove --device $DEVICE
python train.py --model rcnn --embed word2vec --device $DEVICE
python train.py --model rcnn --embed fasttext --device $DEVICE

python train.py --model bert --embed none --device $DEVICE